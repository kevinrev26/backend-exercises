<?php

namespace App\Http\Controllers;
use App\Models\Sale;
use Illuminate\Http\Request;
use Auth;

class SalesController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $sale = Sale::find($id);

        return response()->json($sale);
    }

    public function index()
    {
     
      // This validation should be on a middleware maybe?

      $user = Auth::user();

      if(!$user->hasRole(['sales'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }

     $sales = Sale::all();

     return response()->json($sales);

    }

    public function create(Request $request)
    {
        $user = Auth::user();
        if(!$user->hasRole(['sales'])) {
            return response()->json(['error' => 'Forbidden.'], 403);
         }
           
        $sale = new Sale;

        // Update Product row according to quantity
        // product->available = $product->available - $sale->quantity;
   
          $sale->name= $request->name;
          $sale->price = $request->price;
          $sale->description= $request->description;
          $sale->sku = $request->sku;
          $sale->quantity = $request->quantity;
          $sale->total = $request->price * $request->quantity;
          
          $sale->save();

          
   
          return response()->json($sale);
    }
}