<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsDemoSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit products']);
        Permission::create(['name' => 'delete products']);
        Permission::create(['name' => 'create products']);
        Permission::create(['name' => 'update products']);
        Permission::create(['name' => 'sale products']);

        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'update users']);

        // create roles and assign existing permissions
        $salesRole = Role::create(['name' => 'sales']);
        $adminRole = Role::create(['name' => 'admin']);
        $operatorRole = Role::create(['name' => 'operator']);

        $adminRole->givePermissionTo([
            'edit products', 'delete products', 'create products', 'update products',
            'sale products', 'edit users', 'delete users', 'create users', 'update users'
        ]);

        $salesRole->givePermissionTo('sale products');

        $operatorRole->givePermissionTo([
            'sale products', 'delete products', 'create products', 'update products', 'edit products'
        ]);

        // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'admin',
            'lastname' => 'administrator',
            'email' => 'admin@example.com',
            'password' => app('hash')->make('s3cr3t'),
        ]);
        $user->assignRole($adminRole);

        $user = \App\Models\User::factory()->create([
            'name' => 'operador',
            'lastname' => 'operador1',
            'email' => 'operador@example.com',
            'password' => app('hash')->make('s3cr3t'),
        ]);
        $user->assignRole($operatorRole);

        $user = \App\Models\User::factory()->create([
            'name' => 'cajero',
            'lastname' => 'cajero1',
            'email' => 'cajero@example.com',
            'password' => app('hash')->make('s3cr3t'),
        ]);
        $user->assignRole($salesRole);
    }
}